限流算法  
    1固定窗口算法 。
    2漏桶算法。  
    3令牌桶算法。

策略
    1Google guava  
    
    2 Nginx

    因为Nginx通常作为边缘服务的入口，所以在Nginx上限流会取得比较好的效果，下面介绍一下基于Nginx限流的方案。
连接数限流模块ngx_http_limit_conn_module
ngx_http_limit_conn_module的作用是限制并发访问，包括异常流量、突发流量，甚至是恶意攻击等，它可以根据定义的键来限制每个键值的连接数。
ngx_http_limit_conn_module提供了 limit_conn_zone 和 limit_conn 两个配置参数。其中，limit_conn_zone只能配置在http{}段，而limit_conn则可以配置于http{}、server{}、location{}中。
我们可以在 nginx_conf 的 http{}中加上如下配置实现限制每个用户的并发连接数。其中，$binary_remote_addr表示使用真实 IP 地址作为键；req_one 是我们定义的名字，需要与server{}对应；rate=100r/s表示每秒允许100个请求。
limit_conn_zone $binary_remote_addr zone=req_one:10m rate=100r/s；
配置记录被限流后的日志级别，默认为error级别。
limit_conn_log_level error；
配置被限流后返回的状态码，默认返回503。
limit_conn_status 503；
然后在server{}里加上如下代码。
limit_conn req_one 10；
上述代码限制每个IP并发连接数为10，这里也可以配置多个，例如读的并发连接数是10，写的并发连接数是1。
请求限制模块ngx_http_limit_req_module
ngx_http_limit_conn_module的作用是限制并发访问，而ngx_http_limit_req_module可以为我们限制请求数，该模块可以通过定义的键值来限制请求处理的频率。例如，可以限制来自单个IP地址的请求处理频率。该方法使用了漏桶算法，限制每秒固定处理请求数。如果请求
的频率超过了限制域配置的值，那么请求处理会被延迟或被丢弃，因此所有的请求都是以固定配置的频率被处理的。
首先，在nginx_conf的http{}中加上如下配置实现限制。
limit_req_zone $binary_remote_addr zone=req_one:10m rate=100r/s；
其中，$binary_remote_addr表示使用真实IP地址作为键；req_one是我们定义的名字，需要与server{}对应；rate=100r/s表示每秒允许100个请求。
然后，在server{}里加上如下代码。
limit_req zone=req_one burst=5；
设置桶的容量为5，默认为0。如果配置nodelay，则允许突发，否则保持固定速率。